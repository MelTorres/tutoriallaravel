@component('mail::message')
# Introduction

The body of your message.

<br>-uno
<br>-dos
<br>-tres

@component('mail::button', ['url' => 'https://laracasts.com'])
Comienza a navegar
@endcomponent

@component('mail::panel', ['url' => ''])
Esto es un panel.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
