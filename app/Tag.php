<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
        //n:n
    public function posts(){
      return $this->belongsToMany(Post::class);
    }
    
    
    public function getRouteKeyName(){
      return 'name';
    }
}
